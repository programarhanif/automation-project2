package base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Base {
	
public static WebDriver driver;


    public void navigateURL(String url) {
	driver.get(url);
	
    }

	
	public void getCurrenturl(String url) {
		driver.get(url);
	}
	
	public boolean isDisplayed(By locator) {
		return driver.findElement(locator).isDisplayed();
	
	}
	
	public void click(By locator) {
		driver.findElement(locator).click();
	}
	
	
	public String getText(By locator) {
		return driver.findElement(locator).getText();
	}
	
	public void sendkeys(By locator,String value) {
		driver.findElement(locator).sendKeys(value);
	}
	


}
