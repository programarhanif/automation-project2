$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/Verify_Log_in_Functionality.feature");
formatter.feature({
  "name": "Login functionality",
  "description": "As a User\nI want to login in flipcard\nso that I can see my homepage",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify Login Functionality",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am in Flipcard sites",
  "keyword": "Given "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_am_in_Flipcard_sites()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on cancel button",
  "keyword": "When "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.click_on_cancel_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can see the Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_can_see_the_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/Verify_tabs_In_HomePage_Electronics.feature");
formatter.feature({
  "name": "Eight tabs named in homepage",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify Eight tabs are displayed in home page",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am in Flipcard sites",
  "keyword": "Given "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_am_in_Flipcard_sites()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on cancel button",
  "keyword": "When "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.click_on_cancel_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can see the Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_can_see_the_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click Electronic button",
  "keyword": "And "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_click_Electronic_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can see Electronics homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_can_see_Electronics_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/Verify_tabs_In_HomePage_Man.feature");
formatter.feature({
  "name": "Eight tabs named in homepage",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify Eight tabs are displayed in home page",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am in Flipcard sites",
  "keyword": "Given "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_am_in_Flipcard_sites()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on cancel button",
  "keyword": "When "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.click_on_cancel_button()"
});
formatter.result({
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//button[@class\u003d\u0027_2KpZ6l _2doB4z\u0027]\"}\n  (Session info: chrome\u003d114.0.5735.199)\nFor documentation on this error, please visit: https://www.seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027HANIF-PC\u0027, ip: \u002710.10.10.124\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002711.0.9\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 114.0.5735.199, chrome: {chromedriverVersion: 114.0.5735.90 (386bc09e8f4f..., userDataDir: C:\\Users\\DELL\\AppData\\Local...}, goog:chromeOptions: {debuggerAddress: localhost:60750}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:extension:credBlob: true, webauthn:extension:largeBlob: true, webauthn:extension:minPinLength: true, webauthn:extension:prf: true, webauthn:virtualAuthenticators: true}\nSession ID: 33c19aabce6fc25e4b026f92ff790c1d\n*** Element info: {Using\u003dxpath, value\u003d//button[@class\u003d\u0027_2KpZ6l _2doB4z\u0027]}\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.base/java.lang.reflect.Constructor.newInstance(Constructor.java:490)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:323)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:428)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:315)\r\n\tat step_Defs.Verify_tabs_in_HomePage.click_on_cancel_button(Verify_tabs_in_HomePage.java:26)\r\n\tat ✽.click on cancel button(file:///C:/Java/git/hanifmavenpracticerepo1/Hanif_Maven_Practice_Projects/src/test/resources/features/Verify_tabs_In_HomePage_Man.feature:5)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "I can see the Login page",
  "keyword": "Then "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_can_see_the_Login_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I click Tv button",
  "keyword": "And "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_click_Man_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I can see Tv homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "step_Defs.Verify_tabs_in_HomePage.i_can_see_Man_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.write("this test is  fail ");
formatter.after({
  "status": "passed"
});
});